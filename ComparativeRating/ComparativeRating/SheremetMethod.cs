﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComparativeRating
{
    class SheremetMethod
    {
        public static double[,] StandardizationMatrix(double[,] TableMark)
        {
            double[] MaxMark = SheremetMethod.MaxMark(TableMark);
            double[,] X = new double[TableMark.GetLength(0), TableMark.GetLength(1)]; 
            for (int i = 0; i < TableMark.GetLength(0); i++)
            {
                for (int j = 0; j < TableMark.GetLength(1); j++)
                {
                    X[i, j] = TableMark[i, j] / MaxMark[i];
                }
            }
            return X;
        }

        //Формування масиву максимальних значень показників в групі
        public static double[]MaxMark(double[,] TableMark)
        {
            double[] MaxM = new double[TableMark.GetLength(0)];
            for (int i = 0; i < TableMark.GetLength(0); i++)
            {
                double max = double.MinValue;
                for (int j = 0; j < TableMark.GetLength(1); j++)
                {                  
                    if (TableMark[i, j] > max) max = TableMark[i, j];                    
                }
                MaxM[i] = max;
            }
            return MaxM;
        }

        //Формування рейтингу підприємств
        public static double[] GetRatingEnterprises(double[,] StandardMatrix)
        {
            double[] Rate = new double[StandardMatrix.GetLength(1)];
            for (int i = 0; i < StandardMatrix.GetLength(0); i++)
            {
                for (int j = 0; j < StandardMatrix.GetLength(1); j++)
                {
                    Rate[j] = 0;
                    Rate[j] += Math.Pow(1 - StandardMatrix[i, j], 2);
                }
            }
            for (int i = 0; i < StandardMatrix.GetLength(1); i++)
            {
                Rate[i] = Math.Sqrt(Rate[i]);
            }
            return Rate;
        }

        public static double[] GetRatingEnterprisesAbs(double[,] StandardMatrix)
        {
            double[] Rate = new double[StandardMatrix.GetLength(1)];
            for (int i = 0; i < StandardMatrix.GetLength(0); i++)
            {
                for (int j = 0; j < StandardMatrix.GetLength(1); j++)
                {
                    Rate[j] = 0;
                    Rate[j] += StandardMatrix[i, j];
                }
            }
            return Rate;
        }

        public static double[] GetRatingEnterprisesWeightAbs(double[,] StandardMatrix, double[] WeightCoef)
        {
            double[] Rate = new double[StandardMatrix.GetLength(1)];
            for (int i = 0; i < StandardMatrix.GetLength(0); i++)
            {
                for (int j = 0; j < StandardMatrix.GetLength(1); j++)
                {
                    Rate[j] = 0;
                    Rate[j] += StandardMatrix[i, j] * WeightCoef[i];
                }
            }
            return Rate;
        }

        public static double[]GetRatingEnterprisesWeight(double[,] StandardMatrix, double[] WeightCoef)
        {
            double[] Rate = new double[StandardMatrix.GetLength(1)];
            for (int i = 0; i < StandardMatrix.GetLength(1); i++)
            {
                for (int j = 0; j < StandardMatrix.GetLength(0); j++)
                {
                    Rate[i] = 0;
                    Rate[i] += WeightCoef[j] * (1 - StandardMatrix[j, i]);
                }   
            }
            return Rate;
        }

        public static double[] CalcRate(double[,]TableMark, double[]WeightCoef)
        {
            double[,] nTableMark = SheremetMethod.StandardizationMatrix(TableMark);
            double[] Rate = SheremetMethod.GetRatingEnterprisesWeight(nTableMark, WeightCoef);
            for (int i = 0; i < Rate.Length; i++)
            {
                Rate[i] = Math.Round(Rate[i], 4);
            }
            return Rate;
        }
    }
}
