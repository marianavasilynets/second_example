﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComparativeRating
{
    class IntegralMarksMethod
    {
        public static double Average(int numRow, double[,]Arr)
        {
            double avg = 0;
            for (int i = 0; i < Arr.GetLength(1); i++)
            {
                avg += Arr[numRow, i];   
            }
            return avg / Arr.GetLength(1);
        }

        public static double AverageTwoMarks(int numMark1, int NumMark2, double[,]Arr)
        {
            double avg = 0;
            for (int i = 0; i < Arr.GetLength(1); i++)
            {
                avg += Arr[numMark1, i] * Arr[NumMark2, i];
            }
            return avg / Arr.GetLength(1);
        }

        public static double Variance(int numMark, double[,] Arr)
        {
            double var = 0;
            double avg = Average(numMark, Arr);
            for (int i = 0; i < Arr.GetLength(1); i++)
            {
                var += Math.Pow(Arr[numMark, i] - avg, 2);
            }
            return Math.Sqrt(var);
        }

        public static double CoefCorrelation(int numMark1, int NumMark2,double[,]Arr)
        {
            double avgMark1 = Average(numMark1, Arr);
            double avgMark2 = Average(NumMark2, Arr);
            double avgMark12 = AverageTwoMarks(numMark1, NumMark2, Arr);
            double varMark1 = Variance(numMark1, Arr);
            double varMark2 = Variance(NumMark2, Arr);
            return (avgMark12 - avgMark1 * avgMark2) / (varMark1 * varMark2);
        }

        public static double[,] CorrConnect(double[,]Arr)
        {
            //Підрахунок к-сті зв'язків
            Dictionary<int, int> CountConnect = new Dictionary<int, int>();
            for (int i = 0; i < Arr.GetLength(0); i++)
            {
                CountConnect[i] = 0;   
            }
            for (int i = 0; i < Arr.GetLength(0)-1; i++)
            {
                double coef = CoefCorrelation(i, i + 1, Arr);
                if (Math.Abs(coef) >= 0.9)
                {
                    CountConnect[i] += 1;
                    CountConnect[i + 1] += 1;
                }
            }
            Dictionary<int, int> newCount = new Dictionary<int, int>();
            //    Усунення колінеарності
            for (int i = 0; i < Arr.GetLength(1)-1; i++)
            {
                double coef = CoefCorrelation(i, i + 1, Arr);
                if (Math.Abs(coef) >= 0.9)
                {
                    if (CountConnect[i] > CountConnect[i + 1]) CountConnect.Remove(i);
                    else CountConnect.Remove(i + 1);
                }
            }
            //Формування нового масиву      
            int[] keys = CountConnect.Keys.ToArray();
            double[,] nArr = new double[keys.Length, Arr.GetLength(1)];
            for (int i = 0; i < keys.Length; i++)
            {
                for (int j = 0; j < Arr.GetLength(1); j++)
                {
                    nArr[i, j] = Arr[keys[i], j];
                }
            }
            return nArr;
        }

        public static double[] MaxMark(double[,]TableMark)
        {
            double[] M_mark = new double[TableMark.GetLength(1)];
            for (int i = 0; i < TableMark.GetLength(1); i++)
            {
                double MaxElCol = Double.MinValue;
                for (int j = 0; j < TableMark.GetLength(0); j++)
                {
                    if (TableMark[j, i] > MaxElCol) MaxElCol = TableMark[j, i];
                }
                M_mark[i] = MaxElCol;
            }
            return M_mark;
        }

        public static double[,] StandardizationMatrix(double[,] TableMark)
        {
            double[] M_mark = IntegralMarksMethod.MaxMark(TableMark);
            double[,] Y = new double[TableMark.GetLength(0), TableMark.GetLength(1)];
            for (int i = 0; i < TableMark.GetLength(0); i++)
            {
                for (int j = 0; j < TableMark.GetLength(1); j++)
                {
                    Y[i, j] = TableMark[i, j] / M_mark[j];
                }
            }
            return Y;
        }

        public static double[]MetricMark(double[,]TableMark)
        {
            double[] d = new double[TableMark.GetLength(1)];
            for (int i = 0; i < TableMark.GetLength(1); i++)
            {
                d[i] += Math.Pow(1 - TableMark[0, i], 2);
            }

            for (int i = 0; i < TableMark.GetLength(1); i++)
            {
                d[i] = Math.Sqrt(d[i]);   
            }
            return d;
        }

        private static bool converage(double[] xk, double[] xkp, double eps)
        {
            double norm = 0;
            for (int i = 0; i < xk.Length; i++)
            {
                norm += (xk[i] - xkp[i]) * (xk[i] - xkp[i]);
            }
            if (Math.Sqrt(norm) >= eps) return false;
            return true;
        }

        private static double[] Seidel(double[,] A, double[]f, double[]x0)
        {
            double[] p = new double[x0.Length];
            do
            {
                for (int i = 0; i < x0.Length; i++)
                {
                    p[i] = x0[i];   
                }
                for (int i = 0; i < x0.Length; i++)
                {
                    double var = 0;
                    for (int j = 0; j < i; j++)
                    {
                        var += (A[i, j] * x0[j]);
                    }
                    x0[i] = (f[i] - var) / A[i, i];
                }
            } 
            while (!converage(x0,p,0.000001));
            return x0;
        }

        public static double[] CalcCoefBeta(double[,] TableMark, double[] MetricTable)
        {
            double[,] tempMetric = MathOperations.AdductionVector(MetricTable);
            double[,] z = new double[TableMark.GetLength(1), TableMark.GetLength(0) + 1];
            for (int i = 0; i < z.GetLength(0); i++)
            {
                z[i,0] = 1;
                for (int j = 1; j < z.GetLength(1); j++)
                {
                    z[i, j] = TableMark[j - 1, i];
                }
            }
            double[] beta = new double[TableMark.GetLength(0) + 1];
            for (int i = 0; i < beta.Length; i++)
            {
                beta[i] = 0;
            }

            double[,] temp1 = MathOperations.TranspMatrix(z);
            double[,] temp2 = MathOperations.Multiplication(temp1, z);
            double[,] temp3 = MathOperations.Multiplication(temp1, tempMetric);
            double[] n_temp3 = new double[temp3.GetLength(0)];
            for (int i = 0; i < n_temp3.Length; i++)
            {
                n_temp3[i] = temp3[i, 0];
            }
            double[] n_beta = IntegralMarksMethod.Seidel(temp2, n_temp3, beta);
            return n_beta;
        }

        public static double[]Rate(double[,]TableMark, double[]betaCoef)
        {
            double[] D = new double[TableMark.GetLength(1)];
            for (int i = 0; i < TableMark.GetLength(0); i++)
            {
                for (int j = 0; j < TableMark.GetLength(1); j++)
                {
                    D[j] += betaCoef[i + 1] * TableMark[i, j];  
                }
            }

            for (int i = 0; i < D.Length; i++)
            {
                D[i] += betaCoef[0];  
            }
            return D;
        }

         public static double[] Valuation(double[]Rate, double[,] TableMark)
        {
            double[] ValuationRate = new double[Rate.Length];
            for (int i = 0; i < ValuationRate.Length; i++)
            {
                ValuationRate[i] = Rate[i] / TableMark.GetLength(0);
            }
            return ValuationRate;
        }

        public static double[] CalcRate(double[,] TableMark)
         {
             double[,] nTableMark = IntegralMarksMethod.CorrConnect(TableMark);
             double[] metric = IntegralMarksMethod.MetricMark(nTableMark);
             double[] beta = IntegralMarksMethod.CalcCoefBeta(nTableMark, metric);
             double[] b = IntegralMarksMethod.Rate(nTableMark, beta);
             double[] n_b = IntegralMarksMethod.Valuation(b, nTableMark);
             for (int i = 0; i < n_b.Length; i++)
             {
                Math.Round(n_b[i], 4);
             }
             return n_b;
         }
    }
}
