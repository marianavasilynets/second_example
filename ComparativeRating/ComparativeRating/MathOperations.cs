﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComparativeRating
{
    class MathException:Exception
    {
        public MathException() { }
        public MathException(string message) : base(message) { }
        public MathException(string message, Exception ex) : base(message) { }
        protected MathException(System.Runtime.Serialization.SerializationInfo info, 
            System.Runtime.Serialization.StreamingContext contex)
            : base(info, contex) { }
    }

    class MathOperations
    {
        //Знаходження оберненої матриці
        public static double[,] InverseMatrix(double[,]matrix)
        {
            int n = matrix.GetLength(0);
            double[,] E = new double[n, n];
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (i == j) E[i, j] = 1;
                    else E[i, j] = 0;
                }
            }
            for (int k = 0; k < n - 1; k++)
            {
                for (int j = k + 1; j < n; j++)
                {
                    matrix[k, j] /= matrix[k, k];
                }
                for (int z = 0; z < n; z++)
                {
                    E[k, z] /= matrix[k, k];
                }
                for (int i = k + 1; i < n; i++)
                {
                    for (int j = k + 1; j < n; j++)
                    {
                        matrix[i, j] = matrix[i, j] - matrix[i, k] * matrix[k, j];
                    }
                    for (int z = 0; z < n; z++)
                    {
                        E[i, z] = E[i, z] - matrix[i, k] * E[k, z];
                    }
                }
            }
            double[,] O = new double[n, n];
            for (int j = 0; j < n; j++)
            {
                O[n - 1, j] = E[n - 1, j] / matrix[n - 1, n - 1];
            }
            for (int z = 0; z < n; z++)
            {
                for (int i = n - 2; i >= 0; i--)
                {
                    double s = 0;
                    for (int j = i + 1; j < n; j++)
                    {
                        s += matrix[i, j] * O[j, z];
                    }
                    O[i, z] = E[i, z] - s;
                }
            }
            return O;
            //double[,] M_Obr = new double[arr.GetLength(0), arr.GetLength(0)];
            //for (int i = 0; i < arr.GetLength(0); i++)
            //{
            //    for (int j = 0; j < arr.GetLength(0); j++)
            //    {
            //        if (i == j) M_Obr[i, j] = 1;
            //        else M_Obr[i, j] = 0;
            //    }
            //}
            //double r, res;
            //for (int k = 0; k < M_Obr.GetLength(0); k++)
            //{
            //    r = 1 / arr[k, k];
            //    for (int j = 0; j < M_Obr.GetLength(0); j++)
            //    {
            //        arr[k, j] *= r;
            //        M_Obr[k, j] *= r;
            //    }
            //    for (int i = k+1; i < M_Obr.GetLength(0); i++)
            //    {
            //        res = arr[i, k];
            //        for (int z = 0; z < M_Obr.GetLength(0); z++)
            //        {
            //            arr[i, z] = arr[i, z] - arr[k, z] * res;
            //            M_Obr[i, z] = M_Obr[i, z] - M_Obr[k, z] * res;
            //        }
            //    }
            //}
            //for (int k = M_Obr.GetLength(0)-1; k >=0; k--)
            //{
            //    for (int i = k-1; i >=0; i--)
            //    {
            //        res = arr[i, k];
            //        for (int z = 0; z < M_Obr.GetLength(0); z++)
            //        {
            //            arr[i, z] = arr[i, z] - arr[k, z] * res;
            //            M_Obr[i, z] = M_Obr[i, z] - M_Obr[k, z] * res;
            //        }
            //    }
            //}
            //return M_Obr;
        }

        //Транспонування матриці
        public static double[,] TranspMatrix(double[,] arr)
        {
            int CountRow = arr.GetLength(0);
            int CountColumn = arr.GetLength(1);
            double[,] mT = new double[CountColumn, CountRow];
            for (int i = 0; i < CountColumn; i++)
            {
                for (int j = 0; j < CountRow; j++)
                {
                    mT[i, j] = arr[j, i];
                }
            }
            return mT;
        }

        //Приведення одновимірного масиву до вигляду вектора-стовпця
        public static double[,] AdductionVector(double[] vect)
        {
            double[,] col = new double[vect.Length, 1];
            for (int i = 0; i < vect.Length; i++)
            {
                col[i, 0] = vect[i];
            }
            return col;
        }

        //Множення матриць
        public static double[,] Multiplication(double[,] Matrix1, double[,] Matrix2)
        {
            double[,] r = new double[Matrix1.GetLength(0), Matrix2.GetLength(1)];
            try
            {
                if (Matrix1.GetLength(1) != Matrix2.GetLength(0)) throw new MathException("Матриці не можна перемножити");
                for (int i = 0; i < Matrix1.GetLength(0); i++)
                {
                    for (int j = 0; j < Matrix2.GetLength(1); j++)
                    {
                        for (int k = 0; k < Matrix2.GetLength(0); k++)
                            r[i, j] += Matrix1[i, k] * Matrix2[k, j];
                    }
                }
            }
            catch (MathException e)
            {
                Console.WriteLine(e.Message);
            }
            return r;
        }
    }
}
