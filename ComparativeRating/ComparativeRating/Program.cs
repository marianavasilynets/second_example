﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;

namespace ComparativeRating
{
    class Program
    {
        public static double[,] ReadingExcelTo2DArray(string pathToFile,string StartCellD, string FinCellD, int numList)
        {
            Excel.Application ObjExcel = new Excel.Application();
            Excel.Workbook ObjWorkBook = ObjExcel.Workbooks.Open(pathToFile, 0, false, 5, "", "", false, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "", true, false, 0, true, false, false);
            Excel.Worksheet ObjWorkSheet;
            ObjWorkSheet = (Excel.Worksheet)ObjWorkBook.Sheets[numList];
            Excel.Range rng = ObjWorkSheet.get_Range(StartCellD, FinCellD);
            var dataArr = (object[,])rng.Value;
            dataArr.ToString();
            ObjWorkBook.Close(true);
            ObjExcel.Quit();
            double[,] data = new double[dataArr.GetLength(0), dataArr.GetLength(1)];
            for (int i = 1; i <= data.GetLength(0); i++)
            {
                for (int j = 1; j <= data.GetLength(1); j++)
                {
                    string s = dataArr[i, j].ToString();
                    s.Replace(',', '.');
                    data[i - 1, j - 1] = Convert.ToDouble(s);
                }
            }
            return data;
        }
        static void Main(string[] args)
        {
            string pathToFile = @"D:\Курсовві\Курсова. Прикладна математика(3)\Comparative Rate.xlsx";
            double[,] Mark_2007 = ReadingExcelTo2DArray(pathToFile, "D2", "F43", 1);
            double[,] Mark_2008 = ReadingExcelTo2DArray(pathToFile, "D2", "F43", 2);
            double[,] Mark_2009 = ReadingExcelTo2DArray(pathToFile, "D2", "F43", 3);
            double[,] Mark_2010 = ReadingExcelTo2DArray(pathToFile, "D2", "F43", 4);

            double[,] K = ReadingExcelTo2DArray(pathToFile, "D2", "D43", 5);
            double[] coef = new double[K.GetLength(0)];
            for (int i = 0; i < K.GetLength(0); i++)
			{
                coef[i] = K[i, 0];
			}

            double[] Sheremet_2007 = SheremetMethod.CalcRate(Mark_2007, coef);
            double[] Sheremet_2008 = SheremetMethod.CalcRate(Mark_2008, coef);
            double[] Sheremet_2009 = SheremetMethod.CalcRate(Mark_2009, coef);
            double[] Sheremet_2010 = SheremetMethod.CalcRate(Mark_2010, coef);
            Console.WriteLine("Обчислення рейтингу пiдприємств за методом Баканова i Шеремета\n");
            Console.WriteLine("Вiдхилення рейтингу пiдприємств вiд рейтингу \"iдеального\" пiдприємства\n");
            Console.Write("2007:\t ЕВК - {0}\t Перспектива - {1}\t К'Лен - {2}\n", Sheremet_2007[0], Sheremet_2007[1], Sheremet_2007[2]);
            Console.Write("2008:\t ЕВК - {0}\t Перспектива - {1}\t К'Лен - {2}\n", Sheremet_2008[0], Sheremet_2008[1], Sheremet_2008[2]);
            Console.Write("2009:\t ЕВК - {0}\t Перспектива - {1}\t К'Лен - {2}\n", Sheremet_2009[0], Sheremet_2009[1], Sheremet_2009[2]);
            Console.Write("2010:\t ЕВК - {0}\t Перспектива - {1}\t К'Лен - {2}\n\n", Sheremet_2010[0], Sheremet_2010[1], Sheremet_2010[2]);
            Console.WriteLine("****************************************************************");

            Console.WriteLine("Обчислення рейтингу пiдприємств за методом iнтегральної рейтингової оцiнки\n");
            double[] IntegralM_2007 = IntegralMarksMethod.CalcRate(Mark_2007);
            double[] IntegralM_2008 = IntegralMarksMethod.CalcRate(Mark_2008);
            double[] IntegralM_2009 = IntegralMarksMethod.CalcRate(Mark_2009);
            double[] IntegralM_2010 = IntegralMarksMethod.CalcRate(Mark_2010);
            Console.WriteLine("Рейтинги пiдприємств:\n");
            Console.Write("2007:\t ЕВК - {0}\t Перспектива - {1}\t К'Лен - {2}\n", IntegralM_2007[0], IntegralM_2007[1], IntegralM_2007[2]);
            Console.Write("2008:\t ЕВК - {0}\t Перспектива - {1}\t К'Лен - {2}\n", IntegralM_2008[0], IntegralM_2008[1], IntegralM_2008[2]);
            Console.Write("2009:\t ЕВК - {0}\t Перспектива - {1}\t К'Лен - {2}\n", IntegralM_2009[0], IntegralM_2009[1], IntegralM_2009[2]);
            Console.Write("2010:\t ЕВК - {0}\t Перспектива - {1}\t К'Лен - {2}\n\n", IntegralM_2010[0], IntegralM_2010[1], IntegralM_2010[2]);
            Console.ReadLine();
        }
    }
}
